package edu.westga.cs3110.unicoder.model;

/**
 * This class takes a hex string and allows for UTF32, UTF16, and UTF8 encoding.
 * 
 * @author codyvollrath
 *
 */
public class Codepoint {
	
	private String hexadecimalString;
	
	private static final String REGEX_ENSURE_MAX_IS_INT_MAX_VALUE = "0*((1[0-9A-F]{1,5})|([0-9A-F]{1,5}))";
	private static final int RADIX = 16;
	
	private static final int MIN = 0;
	
	private static final int MAX_SINGLE_BYTE_UTF8 = 0x7F;
	
	private static final int MIN_TWO_BYTE_UTF8 = 0x80;
	private static final int MAX_TWO_BYTE_UTF8 = 0X7FF;
	
	private static final int MIN_THREE_BYTE_UTF8 = 0x800;
	private static final int MAX_THREE_BYTE_UTF8 = 0xFFFF;
	
	private static final int FIRST_MAX_TWO_BYTE_UTF16 = 0xD7FF;
	
	private static final int SECOND_MIN_TWO_BYTE_UTF16 = 0xE000;
	private static final int SECOND_MAX_TWO_BYTE_UTF16 = 0xFFFF;
	
	
	/**
	 *  Initializes the Codepoint class
	 *  
	 * @precondition hexString != null AND !hexString.isEmpty() 
	 * AND hexString uses only hexadecimal values 
	 * AND hexString hex representation does not exceed Integer.MAX_VALUE
	 * @postcondition this.hexadecimalString == hexString
	 * @param hexString the string to encode as a unicode value
	 */
	public Codepoint(String hexString) {
		
		if (hexString == null) {
			throw new IllegalArgumentException("hexString can not be null");
		}
		
		if (hexString.isEmpty()) {
			throw new IllegalArgumentException("hexString can not be empty");
		}
		
		hexString = hexString.toUpperCase();
		
		boolean isHexadecimalInvalid = !hexString.matches(REGEX_ENSURE_MAX_IS_INT_MAX_VALUE);
		
		if (isHexadecimalInvalid) {
			throw new IllegalArgumentException("hexString is not a valid codepoint");
		}
		
		this.hexadecimalString = hexString;
	}
	
	/**
	 * Gets the hexadecimal String
	 * 
	 * @return the hex string
	 */
	public String getHexadecimalString() {
		
		return this.hexadecimalString;
		
	}
	
	/**
	 * Converts the hexadecimal String to a UTF32 encoded representation
	 * 
	 * @return the encoded hex String as a UTF32 encoded hex string
	 */
	public String toUTF32() {
		
		int valueOfHex = this.getHexValueAsInt();
		
		return String.format("%08X", valueOfHex);

	}
	
	/**
	 * Converts the hexadecimal String to a UTF16 encoded representation
	 * 
	 * @return the encoded hex String as a UTF16 encoded hex String
	 */
	public String toUTF16() {
		int valueOfHex = this.getHexValueAsInt();
		
		if (this.isUTF16TwoByte(valueOfHex)) {
			
			return String.format("%04X", valueOfHex);
			
		} else if (this.isUTF16TwoByteEncodingError(valueOfHex)) {
			
			return null;
			
		} else {
			
			valueOfHex = this.encodeFourByteUTF16(valueOfHex);
			
			return this.getHexStringOfIntegerValue(valueOfHex);
		}
	}

	/**
	 * Converts the hexadecimal String to a UTF8 encoded representation
	 * 
	 * @return the encoded hex String as a UTF8 encoded hex String
	 */
	public String toUTF8() {
		
		int valueOfHex = this.getHexValueAsInt();
		
		if (this.isUTF8SingleByte(valueOfHex)) {
			
			return this.getHexStringOfIntegerValue(valueOfHex);
			
		} else if (this.isUTF8TwoByte(valueOfHex)) {
			
			valueOfHex = this.encodeTwoByteUTF8(valueOfHex);
			
			return this.getHexStringOfIntegerValue(valueOfHex);
			
		} else if (this.isUTF8ThreeByte(valueOfHex)) {
			
			valueOfHex = this.encodeThreeByteUTF8(valueOfHex);
			
			return this.getHexStringOfIntegerValue(valueOfHex);
			
		} else {
			
			valueOfHex = this.encodeFourByteUTF8(valueOfHex);
				
			return this.getHexStringOfIntegerValue(valueOfHex);
		}
	
	}

	private int getHexValueAsInt() {
		
		return Integer.parseUnsignedInt(this.hexadecimalString, RADIX);
		
	}
	
	private boolean isUTF16TwoByte(int valueOfHex) {
		
		boolean isValueOfHexWithinFirstRange = valueOfHex >= MIN && valueOfHex <= FIRST_MAX_TWO_BYTE_UTF16;
		boolean isValueOfHexWithinSecondRange = valueOfHex >= SECOND_MIN_TWO_BYTE_UTF16 && valueOfHex <= SECOND_MAX_TWO_BYTE_UTF16;
		
		return isValueOfHexWithinFirstRange || isValueOfHexWithinSecondRange;
		
	}
	
	private boolean isUTF16TwoByteEncodingError(int valueOfHex) {
		
		return valueOfHex > FIRST_MAX_TWO_BYTE_UTF16 && valueOfHex < SECOND_MIN_TWO_BYTE_UTF16;
	}

	private boolean isUTF8SingleByte(int valueOfHex) {
		
		
		return valueOfHex >= MIN && valueOfHex <= MAX_SINGLE_BYTE_UTF8;
		
	}
	
	private boolean isUTF8TwoByte(int valueOfHex) {
		
		return valueOfHex >= MIN_TWO_BYTE_UTF8 && valueOfHex <= MAX_TWO_BYTE_UTF8;
	}
	
	private boolean isUTF8ThreeByte(int valueOfHex) {
	
		return valueOfHex >= MIN_THREE_BYTE_UTF8 && valueOfHex <= MAX_THREE_BYTE_UTF8;
	}
	
	
	private int encodeFourByteUTF16(int valueOfHex) {
		int modifiedValue = valueOfHex - 0x10000;
		
		int upperTenBits = modifiedValue >> 10;
		int lowerTenBits = modifiedValue & 0b1111111111;
		
		int highSurrogate = 0xD800 + upperTenBits;
		int lowSurrogate = 0xDC00 + lowerTenBits;
		
		valueOfHex = highSurrogate << 16 | lowSurrogate;
		
		return valueOfHex;
	}
	
	private int encodeTwoByteUTF8(int valueOfHex) {
		int upperFiveBits = valueOfHex >> 6;
		int firstUTF8Byte = (0b110 << 5) | upperFiveBits;
		
		int lowerSixBits = valueOfHex & 0b111111;
		int secondUTF8Byte = (0b10 << 6) | lowerSixBits;
		
		valueOfHex = firstUTF8Byte << 8 | secondUTF8Byte;
		return valueOfHex;
	}

	private int encodeThreeByteUTF8(int valueOfHex) {
		int upperFourBits = valueOfHex >> 12;
		int firstUTF8Byte = (0b1110 << 4) | upperFourBits;
		
		int middleSixBits = (valueOfHex & 0b111111000000) >> 6;
		int middleUTF8Byte = (0b10 << 6) | middleSixBits;
		
		int lastSixBits = valueOfHex & 0b111111;
		int lastUTF8Byte = (0b10 << 6) | lastSixBits;
		
		valueOfHex = (firstUTF8Byte << 8 | middleUTF8Byte) << 8 | lastUTF8Byte;
		return valueOfHex;
	}

	private int encodeFourByteUTF8(int valueOfHex) {
		int upperThreeBits = valueOfHex >> 18;
		int firstUTF8Byte = (0b11110 << 3) | upperThreeBits;
			
		int firstMiddleSixBits = (valueOfHex & 0b111111000000000000) >> 12;
		int firstMiddleUTF8Byte = 0b10 << 6 | firstMiddleSixBits;
			
		int secondMiddleSixBits = (valueOfHex & 0b111111000000) >> 6;
		int secondMiddleUTF8Byte = 0b10 << 6 | secondMiddleSixBits;
			
		int lastSixBits = valueOfHex & 0b111111;
		int lastUTF8Byte = 0b10 << 6 | lastSixBits;
			
		valueOfHex = ((firstUTF8Byte << 8 | firstMiddleUTF8Byte) << 8 | secondMiddleUTF8Byte) << 8 | lastUTF8Byte;
		return valueOfHex;
	}
	
	private String getHexStringOfIntegerValue(int valueOfHex) {
		
		return String.format("%X", valueOfHex);
	}
	
}
