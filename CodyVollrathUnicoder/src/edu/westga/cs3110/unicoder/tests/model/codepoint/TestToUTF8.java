package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

public class TestToUTF8 {

	@Test
	public void testUTF8SingleByteLowerBound() {
		Codepoint codePoint = new Codepoint("00000");
		
		String result = codePoint.toUTF8();
		
		String expected = "0";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testUTF8SingleByteRandomValue() {
		Codepoint codePoint = new Codepoint("00050");
		
		String result = codePoint.toUTF8();
		
		String expected = "50";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testUTF8SingleByteUpperBound() {
		Codepoint codePoint = new Codepoint("0007F");
		
		String result = codePoint.toUTF8();
		
		String expected = "7F";
		
		assertEquals(expected, result);
	}
	

	
	@Test
	public void testUTF8TwoByteLowerBound() {
		Codepoint codePoint = new Codepoint("80");
		
		String result = codePoint.toUTF8();
		
		String expected = "C280";
		
		assertEquals(expected, result);
	}
	
	@Test 
	public void testUTF8TwoByteRandomValue() {
		Codepoint codePoint = new Codepoint("01a0");
		
		String result = codePoint.toUTF8();
		
		String expected = "C6A0";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testUTF8TwoByteUpperBound() {
		Codepoint codePoint = new Codepoint("7FF");
		
		String result = codePoint.toUTF8();
		
		String expected = "DFBF";
		
		assertEquals(expected, result);
	}
	

	
	@Test 
	public void testUTF8ThreeByteLowerBound() {
		Codepoint codePoint = new Codepoint("800");
		
		String result = codePoint.toUTF8();
		
		String expected = "E0A080";
		
		assertEquals(expected, result);
	}
	
	@Test 
	public void testUTF8ThreeByteRandomValue() {
		Codepoint codePoint = new Codepoint("4CE3");
		
		String result = codePoint.toUTF8();
		
		String expected = "E4B3A3";
		
		assertEquals(expected, result);
	}
	
	@Test 
	public void testUTF8ThreeByteUpperBound() {
		Codepoint codePoint = new Codepoint("FFFF");
		
		String result = codePoint.toUTF8();
		
		String expected = "EFBFBF";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testUTF8FourByteLowerBound() {
		Codepoint codePoint = new Codepoint("10000");
		
		String result = codePoint.toUTF8();
		
		String expected = "F0908080";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testUTF8FourByteRandomValue() {
		Codepoint codePoint = new Codepoint("1ab341");
		
		String result = codePoint.toUTF8();
		
		String expected = "F6AB8D81";
		
		assertEquals(expected, result);
	}
	
	@Test
	public void testUTF8UpperBound() {
		Codepoint codePoint = new Codepoint("1FFFFF");
		
		String result = codePoint.toUTF8();
		
		String expected = "F7BFBFBF";
		
		assertEquals(expected, result);
	}
	
}
