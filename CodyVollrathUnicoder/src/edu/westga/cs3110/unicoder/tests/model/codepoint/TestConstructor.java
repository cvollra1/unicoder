package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

public class TestConstructor {

	@Test
	public void testNullString() {
		assertThrows(IllegalArgumentException.class, ()-> {
			new Codepoint(null);
		});
	}
	
	@Test
	public void testEmptyString() {
		assertThrows(IllegalArgumentException.class, ()-> {
			new Codepoint("");
		});
	}
	
	@Test
	public void testInvalidHexValue() {
		assertThrows(IllegalArgumentException.class, ()-> {
			new Codepoint("XCVDE");
		});
	}
	
	@Test
	public void testInvalidHexValueLength() {
		assertThrows(IllegalArgumentException.class, ()-> {
			new Codepoint("111FF682A");
		});
	}
	
	@Test
	public void testInvalidHexValueLargerThanMaxEncoding() {
		assertThrows(IllegalArgumentException.class, ()-> {
			new Codepoint("2FFFFF");
		});
	}
	
	@Test
	public void testValidHexValueMax() {
		Codepoint codePoint = new Codepoint("1FFFFF");
		
		String result = codePoint.getHexadecimalString();
		
		assertEquals("1FFFFF", result);
	}
	
	@Test
	public void testValidHexWithOneValueBelowMax() {
		Codepoint codePoint = new Codepoint("1FFFFE");
		
		String result = codePoint.getHexadecimalString();
		
		assertEquals("1FFFFE", result);
	}
	
	@Test
	public void testValidHexRandomValue() {
		Codepoint codePoint = new Codepoint("000FFFFF");
		
		String result = codePoint.getHexadecimalString();
		
		assertEquals("000FFFFF", result);
	}
	
	@Test
	public void testValidHexWithLeadingZeros() {
		Codepoint codePoint = new Codepoint("000000006FFFF");
		
		String result = codePoint.getHexadecimalString();
		
		assertEquals("000000006FFFF", result);
	}
	
	@Test
	public void testValidHexValueUpperCase() {
		Codepoint codePoint = new Codepoint("1F682");
		
		String result = codePoint.getHexadecimalString();
		
		assertEquals("1F682", result);
	}
	
	@Test
	public void testValidHexValueLowerCase() {
		Codepoint codePoint = new Codepoint("1f6b2");
		
		String result = codePoint.getHexadecimalString();
		
		assertEquals("1F6B2", result);
	}

}
