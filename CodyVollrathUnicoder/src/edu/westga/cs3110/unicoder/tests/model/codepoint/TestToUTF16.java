package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

class TestToUTF16 {

	@Test
	void testUTF16TwoByteFirstLowerBound() {
		Codepoint codePoint = new Codepoint("0");
		
		String result = codePoint.toUTF16();
		
		String expected = "0000";
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16TwoByteRandomValueFirstRange() {
		String input = "ABCD";
		Codepoint codePoint = new Codepoint(input);
		
		String result = codePoint.toUTF16();
		
		String expected = input;
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16TwoByteFirstUpperBound() {
		String input = "D7FF";
		Codepoint codePoint = new Codepoint(input);
		
		String result = codePoint.toUTF16();
		
		String expected = input;
		
		assertEquals(expected, result);
	}
	

	
	@Test
	void testUTF16TwoByteForbiddenRangeLowerBound() {
		Codepoint codePoint = new Codepoint("D8FF");
		
		String result = codePoint.toUTF16();
		
		String expected = null;
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16TwoByteForbiddenRangeUpperBound() {
		Codepoint codePoint = new Codepoint("DFFF");
		
		String result = codePoint.toUTF16();
		
		String expected = null;
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16TwoByteSecondLowerBound() {
		String input = "E000";
		Codepoint codePoint = new Codepoint(input);
		
		String result = codePoint.toUTF16();
		
		String expected = input;
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16TwoByteRandomValueInSecondRange() {
		String input = "FBCD";
		Codepoint codePoint = new Codepoint(input);
		
		String result = codePoint.toUTF16();
		
		String expected = input;
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16TwoByteSecondUpperBound() {
		String input = "FFFF";
		Codepoint codePoint = new Codepoint(input);
		
		String result = codePoint.toUTF16();
		
		String expected = input;
		
		assertEquals(expected, result);
	}

	@Test
	void testUTF16FourByteLowerBound() {
		Codepoint codePoint = new Codepoint("10000");
		
		String result = codePoint.toUTF16();
		
		String expected = "D800DC00";
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16FourByteRandomValue() {
		Codepoint codePoint = new Codepoint("0183A5");
		
		String result = codePoint.toUTF16();
		
		String expected = "D820DFA5";
		
		assertEquals(expected, result);
	}
	
	@Test
	void testUTF16FourByteUpperBound() {
		Codepoint codePoint = new Codepoint("10FFFF");
		
		String result = codePoint.toUTF16();
		
		String expected = "DBFFDFFF";
		
		assertEquals(expected, result);
	}
	
	
}
