package edu.westga.cs3110.unicoder.tests.model.codepoint;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs3110.unicoder.model.Codepoint;

class TestToUTF32 {

	@Test
	void testGhostEmojiHexValueToUTF32() {
		Codepoint codePoint = new Codepoint("1F47B");
		
		String result = codePoint.toUTF32();
		String expected = "0001F47B";
		assertEquals(expected, result);
	}
	
	@Test
	void testEnglishCharacterUHexValueToUTF32() {
		Codepoint codePoint = new Codepoint("55");
		
		String result = codePoint.toUTF32();
		String expected = "00000055";
		assertEquals(expected, result);
	}
	
	@Test
	void testNonEnglishCharacterHexValueToUTF32() {
		Codepoint codePoint = new Codepoint("FDFD");
		
		String result = codePoint.toUTF32();
		String expected = "0000FDFD";
		assertEquals(expected, result);
	}
	

}
